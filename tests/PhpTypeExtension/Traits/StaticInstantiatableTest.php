<?php
/**
 * 静的ファクトリメソッドトレイト ユニットテスト
 */
namespace Tests\PhpTypeExtension\Traits;

use Tests\PhpTypeExtension\Traits\StaticInstantiatable\TestObject;

/**
 * @coversDefaultClass PhpTypeExtension\Traits\StaticInstantiatable
 */
final class StaticInstantiatableTest
    extends \PHPUnit_Framework_TestCase
{

    /**
     * @covers ::instance
     */
    public function test_instantiate ()
    {
        /* Arrange */
        $id = 1;
        $name = 'test name';
        $remarks = 'test remarks.';
        /* Act */
        $to = TestObject::instance($id, $name, $remarks);
        /* Assert */
        $this->assertEquals($id, $to->id);
        $this->assertEquals($name, $to->name);
        $this->assertEquals($remarks, $to->remarks);
    }

}

namespace Tests\PhpTypeExtension\Traits\StaticInstantiatable;

use PhpTypeExtension\Traits;

class TestObject
{
    use Traits\ReadOnlyFieldsDefinitionable,
        Traits\StaticInstantiatable;

    private $id;
    private $name;
    private $remars;

    private function __construct ($id, $name, $remarks = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->remarks = $remarks;
    }

}
