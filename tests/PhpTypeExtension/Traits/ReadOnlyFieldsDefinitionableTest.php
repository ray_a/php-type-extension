<?php
/**
 * 読み取り専用プロパティトレイト ユニットテスト
 */
namespace Tests\PhpTypeExtension\Traits;

use Tests\PhpTypeExtension\Traits\ReadOnlyFieldsDefinitionable\TestObject;

final class ReadOnlyFieldsDefinitionableTest
    extends \PHPUnit_Framework_TestCase
{

    public function test_読み取り専用フィールドが定義できる ()
    {
        /* Arrange */
        $id = 1;
        $name = 'test name';
        $test = new TestObject($id, $name);
        /* Act */
        /* Assert */
        $this->assertEquals($id, $test->id);
        $this->assertEquals($name, $test->name);
    }

    /**
     * @expectedException \RuntimeException
     * @expectedExceptionMessageRegExp /^Undefined property via __get\(\): email$/
     */
    public function test_存在しないフィールドの参照は例外が発生する ()
    {
        /* Arrange */
        $id = 1;
        $name = 'test name';
        $test = new TestObject($id, $name);
        /* Act */
        $email = $test->email;
        /* Assert */
    }

}

namespace Tests\PhpTypeExtension\Traits\ReadOnlyFieldsDefinitionable;

use PhpTypeExtension\Traits;

final class TestObject
{

    use Traits\ReadOnlyFieldsDefinitionable;

    private $id;
    private $name;

    public function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

}
