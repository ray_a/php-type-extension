<?php
namespace Tests\PhpTypeExtension\HttpSession;

use PhpTypeExtension\HttpSession\NamedHttpSession;
use Tests\PhpTypeExtension\HttpSession\NamedHttpSession\TestObject;

/**
 * @coversDefaultClass PhpTypeExtension\HttpSession\NamedHttpSession
 */
final class NamedHttpSessionTest
    extends \PHPUnit_Framework_TestCase
{

    public function __construct (...$args)
    {
        switch (session_status()) {
            case PHP_SESSION_NONE:
                session_start();
        }
        parent::__construct(...$args);
    }

    /**
     * @covers ::__construct
     * @covers ::hasKey
     * @covers ::save
     * @covers ::load
     */
    public function test_saveAndLoad ()
    {
        /* Arrange */
        $namespace = 'testNamespace';
        $namedSession = NamedHttpSession::instance($namespace);
        $id = 1;
        $name = 'test name.';
        $to = TestObject::instance($id, $name);
        $key = 'testObject';
        /* Act */
        $namedSession->save($key, $to);
        $loadedTo = $namedSession->load($key);
        /* Assert */
        $this->assertEquals($to, $loadedTo);
    }

    /**
     * @covers ::__construct
     * @covers ::remove
     */
    public function test_remove ()
    {
        /* Arrange */
        $namespace = 'testNamespace';
        $namedSession = NamedHttpSession::instance($namespace);
        $id = 1;
        $name = 'test name.';
        $to = TestObject::instance($id, $name);
        $key = 'testObject';
        $namedSession->save($key, $to);
        $this->assertTrue($namedSession->hasKey($key));
        /* Act */
        $namedSession->remove($key);
        /* Assert */
        $this->assertFalse($namedSession->hasKey($key));
    }

    /**
     * @covers ::__construct
     * @covers ::pickOut
     */
    public function test_pickOut ()
    {
        /* Arrange */
        $namespace = 'testNamespace';
        $namedSession = NamedHttpSession::instance($namespace);
        $id = 1;
        $name = 'test name.';
        $to = TestObject::instance($id, $name);
        $key = 'testObject';
        $namedSession->save($key, $to);
        $this->assertTrue($namedSession->hasKey($key));
        /* Act */
        $picked = $namedSession->pickOut($key, null);
        /* Assert */
        $this->assertEquals($to, $picked);
        $this->assertFalse($namedSession->hasKey($key));
    }

}

namespace Tests\PhpTypeExtension\HttpSession\NamedHttpSession;

use PhpTypeExtension\Traits;

class TestObject
{
    use Traits\ReadOnlyFieldsDefinitionable,
        Traits\StaticInstantiatable;

    private $id;
    private $name;

    public function __construct ($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

}
