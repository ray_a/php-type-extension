<?php
/**
 * HTTPセッション ユニットテスト
 */
namespace Tests\PhpTypeExtension\HttpSession;

use PhpTypeExtension\HttpSession\HttpSession;
use Tests\PhpTypeExtension\HttpSession\HttpSession\TestObject;

/**
 * @coversDefaultClass PhpTypeExtension\HttpSession\HttpSession
 */
final class HttpSessionTest
    extends \PHPUnit_Framework_TestCase
{

    public function __construct (...$args)
    {
        switch (session_status()) {
            case PHP_SESSION_NONE:
                session_start();
        }
        parent::__construct(...$args);
    }

    /**
     * @covers ::__construct
     * @covers ::save
     * @covers ::load
     * @covers ::getNamespace
     * @covers ::initializeNamespace
     */
    public function test_saveAndLoad ()
    {
        /* Arrange */
        $session = HttpSession::instance();
        $namespace = 'testNamespace';
        $key = 'testKey';
        $id = 1;
        $name = 'test object name';
        $value = TestObject::instance($id, $name);
        /* Act */
        $session->save($namespace, $key, $value);
        $loaded = $session->load($namespace, $key);
        /* Assert */
        $this->assertEquals($value, $loaded);
    }

    /**
     * @covers ::__construct
     * @covers ::saveGlobal
     * @covers ::loadGlobal
     * @covers ::getNamespace
     * @covers ::initializeNamespace
     */
    public function test_saveAndLoadGlobal ()
    {
        /* Arrange */
        $session = HttpSession::instance();
        $key = 'testKey';
        $id = 1;
        $name = 'test object name';
        $value = TestObject::instance($id, $name);
        /* Act */
        $session->saveGlobal($key, $value);
        $loaded = $session->loadGlobal($key);
        /* Assert */
        $this->assertEquals($value, $loaded);
    }

    /**
     * @covers ::__construct
     * @covers ::hasKey
     * @covers ::remove
     */
    public function test_remove ()
    {
        /* Arrange */
        $session = HttpSession::instance();
        $namespaceA = 'testNamespaceA';
        $namespaceB = 'testNamespaceB';
        $key = 'testKey';
        $to1 = TestObject::instance(1, 'test object name 1');
        $to2 = TestObject::instance(2, 'test object name 2');
        $session->save($namespaceA, $key, $to1);
        $session->save($namespaceB, $key, $to2);
        $this->assertTrue($session->hasKey($namespaceA, $key));
        $this->assertTrue($session->hasKey($namespaceB, $key));
        /* Act */
        $session->remove($namespaceB, $key);
        /* Assert */
        $this->assertTrue($session->hasKey($namespaceA, $key));
        $this->assertFalse($session->hasKey($namespaceB, $key));
    }

    /**
     * @covers ::__construct
     * @covers ::hasKeyGlobal
     * @covers ::removeGlobal
     */
    public function test_removeGlobal ()
    {
        /* Arrange */
        $session = HttpSession::instance();
        $key = 'testKey';
        $to = TestObject::instance(1, 'test object name 1');
        $session->saveGlobal($key, $to);
        $namespace = 'testNamespace';
        $session->save($namespace, $key, $to);
        $this->assertTrue($session->hasKey($namespace, $key));
        $this->assertTrue($session->hasKeyGlobal($key));
        /* Act */
        $session->removeGlobal($key);
        /* Assert */
        $this->assertTrue($session->hasKey($namespace, $key));
        $this->assertFalse($session->hasKeyGlobal($key));
    }

    /**
     * @covers ::__construct
     * @covers ::pickOut
     */
    public function test_pickOut ()
    {
        /* Arrange */
        $session = HttpSession::instance();
        $namespace = 'testNamespace';
        $key = 'testKey';
        $to = TestObject::instance(1, 'test object name 1');
        $session->save($namespace, $key, $to);
        $this->assertTrue($session->hasKey($namespace, $key));
        /* Act */
        $picked = $session->pickOut($namespace, $key);
        /* Assert */
        $this->assertEquals($to, $picked);
        $this->assertFalse($session->hasKey($namespace, $key));
    }

    /**
     * @covers ::hasNamespace
     */
    public function test_namespaceSplitted ()
    {
        /* Arrange */
        $session = HttpSession::instance();
        $namespaceA = 'testNamespaceA';
        $namespaceB = 'testNamespaceB';
        $key = 'testKey';
        $to1 = TestObject::instance(1, 'test object name 1');
        $to2 = TestObject::instance(2, 'test object name 2');
        $session->save($namespaceA, $key, $to1);
        $session->save($namespaceB, $key, $to2);
        /* Act */
        $loaded1 = $session->load($namespaceA, $key);
        $loaded2 = $session->load($namespaceB, $key);
        /* Assert */
        $this->assertEquals($to1, $loaded1);
        $this->assertNotEquals($to2, $loaded1);
        $this->assertEquals($to2, $loaded2);
        $this->assertNotEquals($to1, $loaded2);
    }

}

namespace Tests\PhpTypeExtension\HttpSession\HttpSession;

use PhpTypeExtension\Traits;

class TestObject
{

    use Traits\StaticInstantiatable,
        Traits\ReadOnlyFieldsDefinitionable;

    private $id;
    private $name;

    public function __construct ($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

}
