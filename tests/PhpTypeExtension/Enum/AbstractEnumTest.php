<?php
/**
 * 抽象列挙型 ユニットテスト
 */
namespace Tests\PhpTypeExtension\Enum;

use PhpTypeExtension\Enum;

/**
 * @coversDefaultClass PhpTypeExtension\Enum\AbstractEnum
 */
final class ExtendPhpPrimitiveTest
    extends \PHPUnit_Framework_TestCase
{

    /**
     * @group __callStatic
     * @covers ::initialize
     * @covers ::__callStatic
     * @covers ::__construct
     * @covers ::name
     * @covers ::value
     * @covers ::isHitCacheKey
     * @covers ::saveCache
     * @covers ::loadCache
     */
    public function test_列挙名からインスタンスが生成できる ()
    {
        /* Arrange */
        /* Act */
        $blue = TestColorTypes::blue();
        /* Assert */
        $this->assertInstanceOf(TestColorTypes::class, $blue);
        $this->assertEquals('blue', $blue->name());
        $this->assertEquals('#0000ff', $blue->value());
    }

    /**
     * @group __callStatic
     * @covers ::initialize
     * @covers ::__callStatic
     * @covers ::__construct
     * @covers ::value
     */
    public function test_未定義の列挙値には列挙名が設定される ()
    {
        /* Arrange */
        /* Act */
        /* Assert */
        $this->assertEquals('white', TestColorTypes::white()->value());
        $this->assertEquals('red', TestColorTypes::red()->value());
        $this->assertEquals('yellow', TestColorTypes::yellow()->value());
    }

    /**
     * @group __callStatic
     * @covers ::initialize
     * @covers ::__callStatic
     * @covers ::__construct
     */
    public function test_列挙型は常に同じインスタンスとなる ()
    {
        /* Arrange */
        /* Act */
        $green1 = TestColorTypes::green();
        $green2 = TestColorTypes::green();
        /* Assert */
        $this->assertSame($green1, $green2);
    }

    /**
     * @group nameOf
     * @covers ::nameOf
     */
    public function test_列挙名から列挙型を生成できる ()
    {
        /* Arrange */
        /* Act */
        $red = TestColorTypes::nameOf('red');
        $white = TestColorTypes::nameOf('white');
        /* Assert */
        $this->assertSame(TestColorTypes::red(), $red);
        $this->assertSame(TestColorTypes::white(), $white);
    }

    /**
     * @group valueOf
     * @covers ::valueOf
     */
    public function test_列挙値から列挙型を生成できる ()
    {
        /* Arrange */
        /* Act */
        /* Assert */
        $this->assertSame(TestColorTypes::red(), TestColorTypes::valueOf('red'));
        $this->assertSame(TestColorTypes::black(), TestColorTypes::valueOf('#000000'));
    }

    /**
     * @group __callStatic
     * @covers ::__callStatic
     * @covers ::nameOf
     */
    public function test_未定義の列挙名による生成にはnullが返る ()
    {
        /* Arrange */
        /* Act */
        /* Assert */
        $this->assertNull(TestColorTypes::purple());
        $this->assertNull(TestColorTypes::nameOf('purple'));
    }

    /**
     * @group valueOf
     * @covers ::valueOf
     */
    public function test_未定義の列挙値による生成にはnullが返る ()
    {
        /* Arrange */
        /* Act */
        /* Assert */
        $this->assertNull(TestColorTypes::valueOf('#ff00ff'));
    }

    /**
     * @group all
     * @covers ::all
     */
    public function test_全ての列挙型を取得できる ()
    {
        /* Arrange */
        /* Act */
        $all = TestColorTypes::all();
        /* Assert */
        $this->assertEquals(6, count($all));
        array_map(
            function ($enum) {
                $this->assertContains(
                    $enum->name(), ['white', 'black', 'red', 'blue', 'yellow', 'green']
                );
                $this->assertContains(
                    $enum->value(), [
                        'white', '#000000', 'red', '#0000ff', 'yellow', 'green'
                    ]
                );
            }, $all
        );
    }

}

class TestColorTypes
    extends Enum\AbstractEnum
{

    protected static $white;
    protected static $black = '#000000';
    protected static $red;
    protected static $blue = '#0000ff';
    protected static $yellow;
    protected static $green;

}
