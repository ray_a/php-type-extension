<?php
/**
 * 抽象列挙型
 */
namespace PhpTypeExtension\Enum;

abstract class AbstractEnum
{

    /** キャッシュ @var array<string, array> */
    private static $cache = [];

    /** 列挙名 @var string */
    private $name;

    /** 列挙値 @var mixed */
    private $value;

    /**
     * constructor
     * @param string $name 列挙名
     * @param scalar $value 列挙値
     * @return void
     */
    protected function __construct ($name, $value)
    {
        $this->name = $name;
        $this->value = $value;
    }

    /**
     * 列挙名
     * @return string
     */
    public function name ()
    {
        return $this->name;
    }

    /**
     * 列挙値
     * @return scalar
     */
    public function value ()
    {
        return $this->value;
    }

    /**
     * 列挙名から列挙型を生成する
     * @param string $name 列挙名
     * @return self|null
     */
    public static function nameOf ($name)
    {
        self::initialize();
        $all = self::all();
        return array_key_exists($name, $all) ? $all[$name] : null;
    }

    /**
     * 列挙値から列挙型を生成する
     * @param scalar $value 列挙値
     * @return self
     */
    public static function valueOf ($value)
    {
        self::initialize();
        return array_reduce(
            self::all(), function ($carry, $enum) use ($value) {
                if ($value == $enum->value()) $carry = $enum;
                return $carry;
            }, null
        );
    }

    /**
     * 自身の列挙型を全て取得する
     * @return self[]
     */
    public static function all ()
    {
        self::initialize();
        return self::loadCache();
    }

    /**
     * 列挙名によるインスタンス生成をサポート
     * @param string $name メソッド名
     * @param mixed[] $args OPTIONAL メソッド引数
     * @return self
     */
    public static function __callStatic ($name, array $args = [])
    {
        return self::nameOf($name);
    }

    /**
     * 初期化
     * @static
     * @return void
     */
    private static function initialize ()
    {
        /* 生成済であれば何もしない */
        if (self::isHitCacheKey()) return;
        /* リフレクションによるキャッシュリスト生成 */
        $refClass = new \ReflectionClass(static::class);
        $filter = \ReflectionProperty::IS_STATIC | \ReflectionProperty::IS_PROTECTED;
        $enums = array_reduce(
            $refClass->getProperties($filter), function ($carry, $refProperty) {
                $refProperty->setAccessible(true);
                if ($refProperty->isStatic() && $refProperty->isProtected()) {
                    $name = $refProperty->getName();
                    $value = is_null($value = $refProperty->getValue()) ? $name : $value;
                    $carry[$name] = new static($name, $value);
                }
                return $carry;
            }, []
        );
        self::saveCache($enums);
    }

    /**
     * キャッシュが存在するか
     * @static
     * @return boolean
     */
    private static function isHitCacheKey ()
    {
        return array_key_exists(static::class, self::$cache);
    }

    /**
     * 値をキャッシュする
     * @static
     * @param string $cacheKey キー
     * @param string $value 値
     * @return self
     */
    private static function saveCache ($value)
    {
        self::$cache[static::class] = $value;
    }

    /**
     * キャッシュを読み込む
     * @static
     * @param string $cacheKey キー
     * @return mixed
     */
    private static function loadCache ()
    {
        return self::isHitCacheKey() ? self::$cache[static::class] : null;
    }

}
