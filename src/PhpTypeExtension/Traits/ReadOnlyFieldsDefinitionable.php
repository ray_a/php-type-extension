<?php
/**
 * 読み取り専用プロパティトレイト
 */
namespace PhpTypeExtension\Traits;

trait ReadOnlyFieldsDefinitionable
{

    /**
     * 読み取り専用プロパティへのアクセスサポート
     * @param string $name プロパティ名
     * @return mixed
     * @throws \RuntimeException 対応するフィールドが存在しない
     */
    public function __get ($name)
    {
        return \Closure::bind(
            function ($name) {
                if (array_key_exists($name, $vars = get_object_vars($this))) {
                    return $vars[$name];
                }
                $trace = debug_backtrace();
                throw new \RuntimeException("Undefined property via __get(): {$name}");
            }, $this, static::class
        )->__invoke($name);
    }

}
