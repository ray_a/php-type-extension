<?php
/**
 * 静的ファクトリメソッドトレイト
 */
namespace PhpTypeExtension\Traits;

trait StaticInstantiatable
{

    /**
     * ファクトリメソッド
     * @param mixed[] ...$args 引数リスト
     * @return self
     */
    public static function instance (...$args)
    {
        return \Closure::bind(
            function (...$args) {
                return new static(...$args);
            }, null, static::class
        )->__invoke(...$args);
    }

}
