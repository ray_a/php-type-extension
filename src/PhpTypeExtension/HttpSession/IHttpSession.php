<?php
/**
 * HTTPセッションインターフェース
 */
namespace PhpTypeExtension\HttpSession;

interface IHttpSession
{

    function getNamespace ($namespace);

    function hasKey ($namespace, $key);

    function save ($namespace, $key, $value);

    function load ($namespace, $key, $default = null);

    function remove ($namespace, $key);

    function pickOut ($namespace, $key, $default = null);

    function hasKeyGlobal($key);

    function saveGlobal ($key, $value);

    function loadGlobal ($key, $default = null);

    function removeGlobal ($key);

    function pickOutGlobal ($key, $default = null);

}
