<?php
/**
 * 名前付きHTTPセッション
 */
namespace PhpTypeExtension\HttpSession;

use PhpTypeExtension\Traits;

class NamedHttpSession
{

    /** Traits */
    use
        /* 静的ファクトリトレイト */
        Traits\StaticInstantiatable;

    /** 組み込みセッション @var array */
    private $session;

    /** 名前空間名 @var string */
    private $namespace;

    /**
     * constructor
     * @param string $namespace 名前空間名
     * @return void
     */
    public function __construct ($namespace)
    {
        $this->namespace = $namespace;
        if (!array_key_exists($namespace, $_SESSION)) {
            $_SESSION[$namespace] = [];
        }
        $this->session = &$_SESSION[$namespace];
    }

    /**
     * キーを持つか
     * @param string $key HTTPセッションキー
     * @return boolean
     */
    public function hasKey ($key)
    {
        return array_key_exists($key, $this->session);
    }

    /**
     * キーを指定して値を保存する
     * @param string $key HTTPセッションキー
     * @param mixed $value 値
     * @return self
     */
    public function save ($key, $value)
    {
        $this->session[$key] = serialize($value);
        return $this;
    }

    /**
     * キーを指定して値を読み込む
     * @param string $key HTTPセッションキー
     * @param mixed $default OPTIONAL 既定値
     * @return mixed
     */
    public function load ($key, $default = null)
    {
        return $this->hasKey($key)
            ? unserialize($this->session[$key]) : $default;
    }

    /**
     * キーを指定してキーと値のペアをHTTPセッションから除去する
     * @param string $key HTTPセッションキー
     * @return self
     */
    public function remove ($key)
    {
        if ($this->hasKey($key)) {
            unset($this->session[$key]);
        }
        return $this;
    }

    /**
     * キーを指定して値を取り出す キーと値のペアは除去される
     * @param string $key HTTPセッションキー
     * @param mixed $default OPTIONAL 既定値
     * @return mixed
     */
    public function pickOut ($key, $default = null)
    {
        $value = $this->load($key);
        $this->remove($key);
        return $value;
    }

}
