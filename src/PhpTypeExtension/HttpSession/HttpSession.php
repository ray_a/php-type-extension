<?php
/**
 * HTTPセッション
 */
namespace PhpTypeExtension\HttpSession;

use PhpTypeExtension\Traits;

class HttpSession
    implements IHttpSession
{

    /** 規定のグローバル名前空間 @var string */
    const DEFAULT_NAMESPACE = 'global';

    /** Traits */
    use
        /* 静的ファクトリトレイト */
        Traits\StaticInstantiatable;

    /** 名前空間リスト @var string<string, array<string, string>> */
    private $namespaces = [];

    /**
     * constructor
     * @return void
     */
    public function __construct ()
    {
        $this->initializeNamespace(self::DEFAULT_NAMESPACE);
    }

    /**
     * 名前空間に対応した名前付きHTTPセッションを取得する
     * @param string $namespace 名前空間
     * @return NamedHttpSession
     */
    public function getNamespace ($namespace)
    {
        return $this->initializeNamespace($namespace)->namespaces[$namespace];
    }

    /**
     * 指定の名前空間がキーを持つか
     * @param string $namespace 名前空間
     * @param string $key HTTPセッションキー
     * @return boolean
     */
    public function hasKey ($namespace, $key)
    {
        return $this->getNamespace($namespace)->hasKey($key);
    }

    /**
     * 指定の名前空間からキーを指定して値を保存する
     * @param string $namespace 名前空間
     * @param string $key HTTPセッションキー
     * @param mixed $value 値
     * @return self
     */
    public function save ($namespace, $key, $value)
    {
        $this->getNamespace($namespace)->save($key, $value);
        return $this;
    }

    /**
     * 指定の名前空間からキーを指定して値を読み込む
     * @param string $namespace 名前空間
     * @param string $key HTTPセッションキー
     * @param mixed $default OPTIONAL 既定値
     * @return mixed
     */
    public function load ($namespace, $key, $default = null)
    {
        return $this->getNamespace($namespace)->load($key, $default);
    }

    /**
     * 指定の名前空間のキーを指定してキーと値のペアをHTTPセッションから除去する
     * @param string $namespace 名前空間
     * @param string $key HTTPセッションキー
     * @return self
     */
    public function remove ($namespace, $key)
    {
        return $this->getNamespace($namespace)->remove($key);
    }

    /**
     * 指定の名前空間のキーを指定して値を取り出す キーと値のペアは除去される
     * @param string $key HTTPセッションキー
     * @param mixed $default OPTIONAL 既定値
     * @return mixed
     */
    public function pickOut ($namespace, $key, $default = null)
    {
        return $this->getNamespace($namespace)->pickOut($key, $default);
    }

    /**
     * グローバル名前空間ががキーを持つか
     * @param string $namespace 名前空間
     * @param string $key HTTPセッションキー
     * @return boolean
     */
    public function hasKeyGlobal ($key)
    {
        return $this->getNamespace(self::DEFAULT_NAMESPACE)->hasKey($key);
    }

    /**
     * グローバル名前空間にキーを指定して値を保存する
     * @param string $key HTTPセッションキー
     * @param mixed $value 値
     * @return self
     */
    public function saveGlobal ($key, $value)
    {
        $this->getNamespace(self::DEFAULT_NAMESPACE)->save($key, $value);
        return $this;
    }

    /**
     * グローバル名前空間からキーを指定して値を読み込む
     * @param string $key HTTPセッションキー
     * @param mixed $default OPTIONAL 既定値
     * @return mixed
     */
    public function loadGlobal ($key, $default = null)
    {
        return $this->getNamespace(self::DEFAULT_NAMESPACE)->load($key, $default);
    }

    /**
     * グローバル名前空間のキーを指定してキーと値のペアをHTTPセッションから除去する
     * @param string $key HTTPセッションキー
     * @return self
     */
    public function removeGlobal ($key)
    {
        return $this->getNamespace(self::DEFAULT_NAMESPACE)->remove($key);
    }

    /**
     * グローバル名前空間のキーを指定して値を取り出す キーと値のペアは除去される
     * @param string $key HTTPセッションキー
     * @param mixed $default OPTIONAL 既定値
     * @return mixed
     */
    public function pickOutGlobal ($key, $default = null)
    {
        return $this->getNamespace(self::DEFAULT_NAMESPACE)->pickOut($key, $default);
    }

    /**
     * 名前空間を持つか
     * @param string $namespace 名前空間
     * @return boolean
     */
    private function hasNamespace ($namespace)
    {
        return array_key_exists($namespace, $this->namespaces);
    }

    /**
     * 名前空間を初期化する
     * @param string $namespace 名前空間
     * @reutrn self
     */
    private function initializeNamespace ($namespace)
    {
        if (!$this->hasNamespace($namespace)) {
            $this->namespaces[$namespace] =
                NamedHttpSession::instance($namespace);
        }
        return $this;
    }

}
